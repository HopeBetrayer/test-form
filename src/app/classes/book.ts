export class Book {
  constructor(
    public id: number,
    public name: string,
    public author: string,
    public issueYear: number,
    public addDate: Date,
    public review?: string
  ) {}
}

import { FormControl } from '@angular/forms';

import * as moment from 'moment';

// date validator to ensure correct string-date conversion
export function customDateValidator(control: FormControl) {
  const dateString = control.value;
  const dateObject = moment(dateString, 'YYYY-MM-DD');
  if (dateObject.toString() === 'Invalid date') {
    return {
      addDate: {
        date: dateString
      }
    }
  }
  return null;
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { BookComponent } from './components/book/book.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { BooksListComponent } from './components/books-list/books-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { BookInlineComponent } from './components/book-inline/book-inline.component';
import { BookInlineEditComponent } from './components/book-inline-edit/book-inline-edit.component';
import { InMemoryBookService } from './services/in-memory-book.service';
import { environment } from '../environments/environment';

const appRoutes: Routes = [
  {
    path: 'add',
    component: AddBookComponent,
  },
  {
    path: 'books',
    component: BooksListComponent,
  },
  {
    path: '',
    redirectTo: '/add',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BookComponent,
    AddBookComponent,
    BooksListComponent,
    PageNotFoundComponent,
    BookInlineComponent,
    BookInlineEditComponent,
  ],
  imports: [
    BrowserModule,
    SnotifyModule,
    HttpClientModule,
    environment.production ? [] : HttpClientInMemoryWebApiModule.forRoot(InMemoryBookService),
    ReactiveFormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }

import { TestBed, inject } from '@angular/core/testing';

import { InMemoryBookService } from './in-memory-book.service';

describe('InMemoryBookService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InMemoryBookService]
    });
  });

  it('should be created', inject([InMemoryBookService], (service: InMemoryBookService) => {
    expect(service).toBeTruthy();
  }));
});

import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryBookService implements InMemoryDbService {
  createDb() {
    let books = [
      { id: 1, name: 'Fahrenheit 451', author: 'Ray Bradbury', issueYear: 1953, addDate: new Date(), review: '' },
      { id: 2, name: 'The Catcher in the Rye', author: 'Ray Bradbury', issueYear: 1951, addDate: new Date(), review: '' },
      { id: 3, name: 'Fight Club', author: 'Chuck Palahniuk', issueYear: 1996, addDate: new Date(), review: '' },
      { id: 4, name: 'Ants', author: 'Bernard Werber', issueYear: 1991, addDate: new Date(), review: '' },
      { id: 5, name: 'Witcher', author: 'Andrzej Sapkowski', issueYear: 1986, addDate: new Date(), review: '' },
    ];
    return { books };
  }
}

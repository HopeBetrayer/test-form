import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, ReplaySubject, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Book } from '../classes/book';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class BookService{
  private apiURL: string;
  private booksSubject: Subject<Book[]>;
  private booksRequest: Observable<Book[]>;

  constructor(private http: HttpClient) {
    this.booksSubject = new ReplaySubject<Book[]>();
    this.apiURL = environment.apiURL;
  }

  getBooks(): Observable<Book[]> {
    this.booksRequest = this.http.get<Book[]>(this.apiURL);
    this.booksRequest.subscribe(
      result => this.booksSubject.next(result),
      error => this.booksSubject.error(error)
    );
    return this.booksSubject.asObservable();
  }

  addBook(book: Book) {
    return this.http.post<Book>(this.apiURL, book);
  }

  updateBook(book: Book) {
    return this.http.put<Book>(this.apiURL, book);
  }

  deleteBook(book: Book) {
    return this.http.delete<Book>(`${this.apiURL}/${book.id}`);
  }
}

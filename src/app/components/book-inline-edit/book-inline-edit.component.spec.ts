import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookInlineEditComponent } from './book-inline-edit.component';

describe('BookInlineEditComponent', () => {
  let component: BookInlineEditComponent;
  let fixture: ComponentFixture<BookInlineEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookInlineEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookInlineEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as moment from 'moment';

import { Book } from '../../classes/book';
import { customDateValidator } from '../../util/Validators';

@Component({
  selector: 'app-book-inline-edit',
  templateUrl: './book-inline-edit.component.html',
  styleUrls: ['./book-inline-edit.component.css']
})

export class BookInlineEditComponent implements OnInit{
  @Input() book: Book;
  @Output() edit = new EventEmitter<boolean>();
  @Output() onSave = new EventEmitter<Book>();
  @Output() onDelete = new EventEmitter<Book>();

  bookForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  initForm(book) {
    this.bookForm = this.fb.group({
      name: [book.name, Validators.required],
      author: [book.author, Validators.required],
      issueYear: [book.issueYear, Validators.required],
      addDate: [moment(book.addDate).format('YYYY-MM-DD'), Validators.compose([
        Validators.required,
        Validators.pattern(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/ig),
        customDateValidator,
      ])],
      review: ''
    })
  }

  ngOnInit() {
    this.initForm(this.book);
  }

  viewBook() {
    this.edit.emit(false);
  }

  onSubmit() {
    if (!this.bookForm.valid) return;
    const { name, author, issueYear, addDate, review } = this.bookForm.value;
    Object.assign(this.book, { name, author, issueYear, review });
    this.book.addDate = moment(addDate, 'YYYY-MM-DD').toDate();
    this.onSave.emit(this.book);
    this.edit.emit(false);
  }

  removeBook() {
    this.onDelete.emit(this.book);
  }
}

import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Book } from '../../classes/book';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

export class BooksComponent {
  @Input() books: Book[];
  @Output() onBookSave = new EventEmitter<Book>();
  @Output() onBookDelete = new EventEmitter<Book>();

  constructor() { }

  handleBookSave(book: Book) {
    this.onBookSave.emit(book);
  }

  handleBookDelete(book: Book) {
    this.onBookDelete.emit(book);
  }
}

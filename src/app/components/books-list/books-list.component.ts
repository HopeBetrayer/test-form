import { Component, OnInit } from '@angular/core';

import { SnotifyService } from 'ng-snotify';

import { Book } from '../../classes/book';
import { BookService } from '../../services/book.service';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css']
})

export class BooksListComponent implements OnInit {

  books: Book[];

  constructor(
    private bookService: BookService,
    private snotify: SnotifyService
  ) {}

  ngOnInit() {
    this.bookService.getBooks()
                    .subscribe(data => this.books = data);
  }

  saveBook(book: Book) {
    this.bookService.updateBook(book)
                    .subscribe(data => {
                        this.snotify.success(`Info on book ${book.name} has been updated`);
                    });
  }

  promptDeleteBook(book: Book) {
    this.snotify.confirm(`Do you really want to delete book ${book.name}?`, 'Confirm action', {
      closeOnClick: true,
      buttons: [
        { text: 'Yes', action: () => this.deleteBook(book) },
        { text: 'No', action: () => null },
      ],
    });
  }

  deleteBook(book: Book) {
    this.bookService.deleteBook(book)
                    .subscribe(data => {
                        this.books = this.books.filter(listedBook => listedBook.id !== book.id);
                        this.snotify.success(`Book ${book.name} has been removed from collection`);
                    });
  }
}

import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { SnotifyService } from 'ng-snotify';
import * as moment from 'moment';

import { BookService } from '../../services/book.service';
import { Book } from '../../classes/book';
import { customDateValidator } from '../../util/Validators';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})

export class AddBookComponent {

  private bookForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private bookService: BookService,
    private snotify: SnotifyService
  ) {
    this.setupForm();
  }

  setupForm() {
    this.bookForm = this.fb.group({
      name: ['', Validators.required],
      author: ['', Validators.required],
      issueYear: ['', Validators.required],
      addDate: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/ig),
        customDateValidator,
      ])],
      review: ''
    });
  }

  onSubmit() {
    if (!this.bookForm.valid) return;
    const { name, author, issueYear, addDate, review } = this.bookForm.value;
    const newBook = new Book(null, name, author, issueYear, moment(addDate, 'YYYY-MM-DD').toDate(), review);
    const response = this.bookService.addBook(newBook);
    response.subscribe(
      data => {
        this.snotify.success(`Book ${newBook.name} has been added to collection`);
        this.router.navigate(['/books'], {});
      }
    )
  }
}

import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Book } from '../../classes/book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})

export class BookComponent {
  @Input() book: Book;
  @Output() onSave = new EventEmitter<Book>();
  @Output() onDelete = new EventEmitter<Book>();
  
  edit: boolean;

  constructor() {
    this.edit = false;
  }

  onEdit(edit: boolean) {
    this.edit = edit;
  }

  handleSave(book: Book) {
    this.onSave.emit(book);
  }

  handleDelete(book: Book) {
    this.onDelete.emit(book);
  }
}

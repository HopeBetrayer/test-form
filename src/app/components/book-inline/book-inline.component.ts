import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Book } from '../../classes/book';

@Component({
  selector: 'app-book-inline',
  templateUrl: './book-inline.component.html',
  styleUrls: ['./book-inline.component.css']
})

export class BookInlineComponent {
  @Input() book: Book;
  @Output() edit = new EventEmitter<boolean>();
  @Output() onDelete = new EventEmitter<Book>();

  constructor() { }

  editBook() {
    this.edit.emit(true);
  }

  removeBook() {
    this.onDelete.emit(this.book);
  }
}

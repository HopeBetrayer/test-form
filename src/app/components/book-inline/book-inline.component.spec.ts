import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookInlineComponent } from './book-inline.component';

describe('BookInlineComponent', () => {
  let component: BookInlineComponent;
  let fixture: ComponentFixture<BookInlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

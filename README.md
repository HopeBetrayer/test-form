# TestForm

## Install

* run `git clone git@bitbucket.org:HopeBetrayer/test-form.git` to clone via SSH or `git clone https://HopeBetrayer@bitbucket.org/HopeBetrayer/test-form.git` to clone via HTTPS
* run `npm install` to install dependencies
* ensure that you installed Angular CLI by running `npm install -g @angular/cli`

## Starting local server

Run `ng serve` for a local server. Navigate to `http://localhost:4200/`.

## Building bundle

Run `npm run build` to build the project. Outpust are stored in `dist` folder.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

